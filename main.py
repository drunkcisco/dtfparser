from bs4 import BeautifulSoup
import requests
import string
import re
from urllib.parse import urlparse

url = 'https://dtf.ru/'

def awesomeParse(url):
    page = requests.get(url)
    sitename = urlparse(url).netloc
    sitename = '.'.join(sitename.split('.')[-2:])
    filteredNews = []
    awesomeNews = []

    print("Статус страницы: ", page.status_code, "\n")
    soup = BeautifulSoup(page.text, "html.parser")
    allNews = soup.find_all("div", class_="content-title content-title--short l-island-a")

    for data in allNews:
        if data.find('span', class_='content-editorial-tick') is not None:
            filteredNews.append(data.text)
    for data in filteredNews:
        data = re.sub(r"Статьи редакции", "", data)

        if " не " in data:
            data = "Геймеры в ярости: "+data
        else:
            data = sitename+": "+data

        newdata = " ".join(data.split())
        awesomeNews.append(newdata)
    return awesomeNews
        
print ('\n'.join(awesomeParse("https://www.dtf.ru")))
print ('\n')
print ('\n'.join(awesomeParse("https://www.vc.ru")))
